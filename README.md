# X-Kating
> An University of Alicante & Multimedia Engineering videogame project developed by 5 awesome students

This repository is a clone from the original repository where Adrián Francés Lillo, Pablo López Iborra, Laura Hernández Rielo, Alexei Jilinskiy and I, Luis González Aracil, worked and developed X-Kating.

## Game Background
Year 2500 - The Earth is a sad, lonely place, where living is no longer worth a dime... if it wasn't for a group of hallucinogenic mushroom consumers who appeared few years ago. They started gathering followers, making bets on dreadful and dangerous skating races that frequantly ended up with the demise of their participants. Over the years, X-Kating, as it became popularly known, has developed into an illegal extreme sport highly waged. Now hundreds of people devote their lives to it and numerous urban tribes have risen up, gathering up lots of supporters who run and bet, in a desperate attemp to thrive in this **wasted** world.

## Technical Details
X-Kating is written in C++, and uses the following external libraries:
- RakNet: Online gameplay.
- OpenGL: Visual rendering
- Nuklear: GUI and HUD.
- FMOD: Sound & music.
- GLFW:
- SFML: Texture parsing.
- GLM: Mathematical library.
- SDL.
- ASSIMP: Model loader.

While developing X-Kating we also developed our own Physics Engine (LAPAL) and also a Graphics Engine (Red Panda Studio).
